//部门选择
function openDeptSelecter(pid,callBack){
	
	layui.use(['layer','form','table','treeTable'],function () {
	    var form=layui.form;
	    var layer=layui.layer;
	    var table=layui.table;
		var treeTable = layui.treeTable;
		var tableSelectIns;
		
		var selDeptIndex=layer.open({
			type:1,
			title:'选择部门',
			content:'<div style="padding:5px" ><table class="layui-hide" id="deptSelectTable" lay-filter="deptSelectTable"></table></div>',
			area:['700px','600px'],
			btn:['<span class="layui-icon layui-icon-ok"></span>确定'],
			btnAlign:'c',
			yes:function(index){
				//得到选择的ID和名称
				var ckData=tableSelectIns.checkStatus();
				//返回选中的父级id 以及 父级title
				callBack(ckData[0].id,ckData[0].title);
				layer.close(selDeptIndex);
			},
			success:function(index){
				 tableSelectIns=treeTable.render({
						tree: {
							iconIndex: 1,  // 折叠图标显示在第几列
							idName: 'id',  // 自定义id字段的名称
							pidName: 'pid',  // 自定义标识是否还有子节点的字段名称
							isPidData: true  // 是否是pid形式数据
						 },
				        elem: '#deptSelectTable',
						cellMinWidth:true,
				        cols: [
				            {type: "radio"},
				            {field: 'title',  title: '部门名称'},
							{field: 'id', title: 'ID',width: 50, align: "center"},
				            /* {field: 'id', title: 'ID', align: "center"}, */
				            {field: 'remark',  title: '部门备注', align: "center"},
				            {field: 'address',  title: '部门地址', align: "center"},
				        ],
						 reqData: function(data, callback) {
							// 在这里写ajax请求，通过callback方法回调数据
							$.get(api+'dept/loadAllDept', function (res) {
								callback(res.data);  // 参数是数组类型
								// alert(pid);
								tableSelectIns.setChecked([pid]);  // 设置选中数据 即是设置ID 
							});
						}
				    });
			}
		});	
	  })
}

// 菜单选择
function openMenuSelecter(pid,callBack){
	
	layui.use(['layer','form','table','treeTable'],function () {
	    var form=layui.form;
	    var layer=layui.layer;
	    var table=layui.table;
		var treeTable = layui.treeTable;
		var tableSelectIns;
		
		var selDeptIndex=layer.open({
			type:1,
			title:'选择菜单',
			content:'<div style="padding:5px" ><table class="layui-hide" id="deptSelectTable" lay-filter="deptSelectTable"></table></div>',
			area:['750px','600px'],
			btn:['<span class="layui-icon layui-icon-ok"></span>确定'],
			btnAlign:'c',
			yes:function(index){
				//得到选择的ID和名称
				var ckData=tableSelectIns.checkStatus();
				//返回选中的父级id 以及 父级title
				callBack(ckData[0].id,ckData[0].title);
				layer.close(selDeptIndex);
			},
			success:function(index){
				 tableSelectIns=treeTable.render({
						tree: {
							iconIndex: 1,  // 折叠图标显示在第几列
							idName: 'id',  // 自定义id字段的名称
							pidName: 'pid',  // 自定义标识是否还有子节点的字段名称
							isPidData: true  ,// 是否是pid形式数据
							openName: 'spread'   // 自定义默认展开的字段名
						 },
				        elem: '#deptSelectTable',
						cellMinWidth:true,
				        cols: [
							
							{type: "radio"},
				        	{field: 'title',  title: '菜单名称'},
							{field: 'id', title: 'ID',width: 50, align: "center"},
				        	{field: 'type',  title: '类型', align: "center",templet: function (d) {
				        			if (d.type == 'topmenu') {
				        				return '<span class="layui-badge layui-bg-red">顶部菜单</span>';
				        			}else if (d.type == 'leftmenu') {
				        				return '<span class="layui-badge layui-bg-blue">左侧菜单</span>';
				        			} else {
				        				return '<span class="layui-badge layui-bg-molv">权限</span>';
				        			}
				        		}
				        	},
				        	{field: 'typecode',  title: '编码', align: "center"}
				        ],
				         reqData: function(data, callback) {
				        	// 在这里写ajax请求，通过callback方法回调数据 加载treetable数据
				        	$.get(api+'menu/loadMenu', function (res) {
				        		callback(res.data);  // 参数是数组类型
								tableSelectIns.setChecked([pid]);  // 设置选中数据  菜单这里加一个[]  即是设置ID 
				        	});
				        }
				    });
			}
		});	
	  })
}

//菜单与权限选择 //分配权限
function openMenuAndPermissionSelecter(data_table){
	layui.use(['layer','form','table','treeTable'],function () {
	    var form=layui.form;
	    var layer=layui.layer;
	    var table=layui.table;
		var treeTable = layui.treeTable;
		var tableSelectIns;
		
		var selMenuIndex=layer.open({
			type:1,
			title:'选择部门',
			content:'<div style="padding:5px" ><table class="layui-hide" id="selectMenuTable" lay-filter="selectMenuTable"></table></div>',
			area:['700px','600px'],
			btn:['<span class="layui-icon layui-icon-ok"></span>确定'],
			btnAlign:'c',
			yes:function(index){
				//得到选择的ID和名称
				var ckData=tableSelectIns.checkStatus();
				var rid=data_table.id;
				var params="rid="+rid;
				$.each(ckData,function(x,item){
					params+="&mids="+item.id;
				});
				$.get(api+"role/saveRoleMenu",params,function(res){
					layer.msg(res.msg);
				})
			},
			success:function(index){
				 tableSelectIns=treeTable.render({
						tree: {
							iconIndex: 1,  // 折叠图标显示在第几列
							idName: 'id',  // 自定义id字段的名称
							pidName: 'pid',  // 自定义标识是否还有子节点的字段名称
							isPidData: true ,// 是否是pid形式数据
							openName: 'spread'   // 自定义默认展开的字段名
						 },
				        elem: '#selectMenuTable',
						cellMinWidth:true,
				        cols: [
				            {type: "checkbox"},
				            {field: 'title',  title: '名称'},
							{field: 'id', title: 'ID',width: 50, align: "center"},
				            /* {field: 'id', title: 'ID', align: "center"}, */
				            {field: 'type',  title: '类型', align: "center",templet: function (d) {
				            		if (d.type == 'topmenu') {
				            			return '<span class="layui-badge layui-bg-red">顶部菜单</span>';
				            		}else if (d.type == 'leftmenu') {
				            			return '<span class="layui-badge layui-bg-blue">左侧菜单</span>';
				            		} else {
				            			return '<span class="layui-badge layui-bg-molv">权限</span>';
				            		}
				            	}
				            }
				        ],
						 reqData: function(data, callback) {
							// 在这里写ajax请求，通过callback方法回调数据
							$.get(api+'menu/loadAllMenu?available=1', function (res) {
								//渲染上面的下拉树菜单
								callback(res.data);  // 参数是数组类型
								
								//选择中已选择的权限或菜单
								 $.get(api+'role/queryMenuIdsByRid',{id:data_table.id},function(rs){
									 // console.log(rs.data);//[1,3,8]
									 var x=new Array();
									 //遍历菜单和权限
									 $.each(res.data,function(i,i_1){
										 //遍历选中的菜单和权限的ID
									 	$.each(rs.data,function(j,j_1){
									 		if(i_1.id==j_1){
									 			if(!i_1.children){
									 				x.push(j_1);
									 			}
									 		}
									 	})
									 });
									 tableSelectIns.setChecked(x);  // 设置选中数据
								 })
							});
						}
				    });
			}
		});	
	  })
}