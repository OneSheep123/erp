//访问接口的IP 服务器上若没有配置负载均衡 这里82要改回后端接口API的8081;若配置了则为nginx的端口82 进行反向代理 负载均衡
var api = "http://127.0.0.1:8080/api/";	
//访问图片地址
var baseUrl = "http://212.64.58.72:81/"
//项目IP
var erpip = "http://127.0.0.1:8848";
//项目登陆页面
var erplogin = "/ERP-WEB/login.html";
//如果访问登陆页面这外的页面并且还没有登陆成功之后写入cookie的token就转到登陆页面
var token = $.cookie('TOKEN');
//设置全局ajax前置拦截
$.ajaxSetup({
	headers:{
		'TOKEN': token //每次ajax请求时把token带过去
	}
})

//当token不存在 同样不是登录页面时
if(token==undefined){
	if(window.location!= erpip + erplogin){
		window.top.location.href = erplogin;
	}
}else{
	//当token存在 而且不是登录页面时 通过后台当前的token是否有效
	if(window.location!= erpip + erplogin){
		$.ajax({
			url:api+"login/checkLogin",
			async:true, //同步请求
			type:'post',
			dataType:'json',//返回类型json
			success:function(res){
				if(res.code==-1){
					window.top.location.href = erplogin;
				}
			},
			error:function(res){
				window.top.location.href = erplogin;
			}
			
		})
	}
}

var pers = localStorage.getItem("permissions");
//如果是超级用户 则不用权限判断
var usertype = localStorage.getItem("usertype");
if(usertype == 1){
	if(pers!=null){
		//permissions包涵所有的权限
		var permissions = pers.split(",");
		console.log(permissions);
		//部门权限 
		if(permissions.indexOf("dept:add")<0){
			$(".dept_btn_add").hide();
		}
		if(permissions.indexOf("dept:delete")<0){
			$(".dept_btn_delete").hide();
		}
		if(permissions.indexOf("dept:update")<0){
			$(".dept_btn_update").hide();
		}
		//菜单权限
		if(permissions.indexOf("menu:add")<0){
			$(".menu_btn_add").hide();
		}
		if(permissions.indexOf("menu:delete")<0){
			$(".menu_btn_delete").hide();
		}
		if(permissions.indexOf("menu:update")<0){
			$(".menu_btn_update").hide();
		}
	}else{
		//权限为空时 表示 权限都无 都隐藏
		//添加按钮隐藏
		$("a[class$='_btn_add']").hide();
		$("button[class$='_btn_add']").hide();
		$("input[class$='_btn_add']").hide();
		//修改按钮隐藏
		$("a[class$='_btn_update']").hide();
		$("button[class$='_btn_update']").hide();
		$("input[class$='_btn_update']").hide();
		//删除按钮
		$("a[class$='_btn_delete']").hide();
		$("button[class$='_btn_delete']").hide();
		$("input[class$='_btn_delete']").hide();
		//分配按钮
		$("a[class$='_btn_dispatch']").hide();
		$("button[class$='_btn_dispatch']").hide();
		$("input[class$='_btn_dispatch']").hide();
		//分配按钮
		$("a[class$='_btn_reset']").hide();
		$("button[class$='_btn_reset']").hide();
		$("input[class$='_btn_reset']").hide();
	}
	
}

//登录名字设置
var username = localStorage.getItem("username");
//用户ID
var userid = localStorage.getItem("userid");
$(".LOGIN_NAME").html(username);
$(".LOGIN_NAME").val(username);
$(".LOGIN_ID").val(userid);